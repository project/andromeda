<?php

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/
function andromeda_form_system_theme_settings_alter(&$form, $form_state) {

  $form['andromeda_theme_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Andromeda Theme Settings'),
  );
  
  $form['andromeda_theme_settings']['show_front_page_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show page tile on front page'),
    '#default_value' => theme_get_setting('andromeda_show_title_front_page'),
  );

  // Return the additional form widgets
  return $form;
}