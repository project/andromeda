<?php
// $Id$

/**
 * Add body classes if certain regions have content.
 */
function andromeda_preprocess_html(&$variables) {
  
  $settings = variable_get('theme_andromeda_settings', '');
  
  $variables['classes_array'] = array();
  
  if ($variables['is_front']) {
    $variables['classes_array'][] = 'front';
  } else {
    $variables['classes_array'][] = 'not-front';
  }
  
  if (!empty($variables['page']['banner'])) {
    $variables['classes_array'][] = 'with-banner';
  }
  
  if (!empty($variables['page']['sidebar'])) {
    $variables['classes_array'][] = 'with-sidebar';
  }
  
  if ($settings['toggle_slogan']) {
    $variables['classes_array'][] = 'with-slogan';
  }
  
  if (theme_get_setting('andromeda_show_title_front_page') && $variables['is_front']) {
    $variables['classes_array'][] = 'with-front-page-title';
  }
}

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function andromeda_preprocess_block(&$variables, $hook) {
  // Classes describing the position of the block within the region.
  if ($variables['block_id'] == 1) {
    $variables['classes_array'][] = 'first';
  }
  // The last_in_region property is set in zen_page_alter().
  if (isset($variables['block']->last_in_region)) {
    $variables['classes_array'][] = 'last';
  }
  $variables['classes_array'][] = $variables['block_zebra'];

  $variables['title_attributes_array']['class'][] = 'block-title';
}

/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered.
 */
function andromeda_preprocess_node(&$variables) {
  switch ($variables['type']) {
    case 'blog' :
      if (array_key_exists('field_blog_picture', $variables['content'])) {
        $variables['classes_array'][] = 'node-blog-has-picture';
      }
      $variables['title_classes'] = 'node-title';
      break;
  }
}
